
### Setup Terraform and Create Instance

Following Terraform configuration is to demonstrate the following:
   * Provision and create an OpenStack instance
   * Install Software (e.g. docker, chrony)
   * Configure the system (i.e. Setup Asia/Singapore timezone. Refer to [bootstrap](openstack/bootstrap.sh)) 

1. Install Terraform. Refer to https://learn.hashicorp.com/terraform/getting-started/install.html

1. Create ``deploy.tf`` from ``deploy.tf.sample`` and change the following accordingly:
   * **image_name** -
      ``` 
      $ openstack image list  - Can use following command or check OpenStack dashboard
      +--------------------------------------+------------------------------------------+--------+
      | ID                                   | Name                                     | Status |
      +--------------------------------------+------------------------------------------+--------+
      | c19eb4bc-xxxx-yyyy-zzzz-5a148d4dc680 | CentOS7-image                            | active |
      +--------------------------------------+------------------------------------------+--------+
      ```
   * **availability_zone** - Can use following command or check OpenStack dashboard
      ```
      $ openstack availability zone list --insecure
      +-----------+-------------+
      | Zone Name | Zone Status |
      +-----------+-------------+
      | Zone-A    | available   |
      +-----------+-------------+
      ```
   * **flavor_id** - Can use following command
      ```
      $ openstack flavor list
      +--------------------------------------+---------------------+-------+------+-----------+-------+-----------+
      | ID                                   | Name                |   RAM | Disk | Ephemeral | VCPUs | Is Public |
      +--------------------------------------+---------------------+-------+------+-----------+-------+-----------+
      | 1                                    | m1.tiny             |   512 |    1 |         0 |     1 | True      |
      | 2                                    | m1.small            |  2048 |   20 |         0 |     1 | True      |
      | 3                                    | m1.medium           |  4096 |   40 |         0 |     2 | True      |
      +--------------------------------------+---------------------+-------+------+-----------+-------+-----------+
      ```
   * **key_pair** - Check OpenStack dashboard -> Access & Security -> Key Pairs
   * **security_groups** - Can use following command or check OpenStack dashboard
      ```
      $ openstack security group list
      +--------------------------------------+----------------+------------------------+----------------------------------+
      | ID                                   | Name           | Description            | Project                          |
      +--------------------------------------+----------------+------------------------+----------------------------------+
      | 57128e06-xxxx-yyyy-zzzz-af36fc15cdbb | default        | Default security group | ce0dcc9fd4be4544be2c018867a1f662 |
      +--------------------------------------+----------------+------------------------+----------------------------------+
      ```
   
1. Create ``variables.tf`` from ``variables.tf.sample`` and change the following accordingly:
   * openstack_user_name
   * openstack_tenant_name
   * openstack_password
   * openstack_auth_url
   * openstack_keypair
   * tenant_network

1. Run ``terraform init`` to initialize terraform
   ```
   $ terraform init
   
   Initializing provider plugins...
   
   The following providers do not have any version constraints in configuration,
   so the latest version was installed.
   
   To prevent automatic upgrades to new major versions that may contain breaking
   changes, it is recommended to add version = "..." constraints to the
   corresponding provider blocks in configuration, with the constraint strings
   suggested below.
   
   * provider.openstack: version = "~> 1.14
   
   ... truncated ...
   ```
   
1. Run ``terraform plan`` to create an execution plan.
   ```
   $ terraform plan
   Refreshing Terraform state in-memory prior to plan...
   The refreshed state will be used to calculate this plan, but will not be
   persisted to local or remote state storage.
   
   ------------------------------------------------------------------------
   
   An execution plan has been generated and is shown below.
   Resource actions are indicated with the following symbols:
     + create
   
   Terraform will perform the following actions:
   
     + openstack_compute_instance_v2.web
         id:                         <computed>
         access_ip_v4:               <computed>
         access_ip_v6:               <computed>
         all_metadata.%:             <computed>
         availability_zone:          "Zone-A"
         flavor_id:                  "2"
         flavor_name:                <computed>
         force_delete:               "false"
         image_id:                   <computed>
         image_name:                 "CentOS7-image"
         key_pair:                   "mykeypair"
         name:                       "web-01"
         network.#:                  "1"
         network.0.access_network:   "false"
         network.0.fixed_ip_v4:      <computed>
         network.0.fixed_ip_v6:      <computed>
         network.0.floating_ip:      <computed>
         network.0.mac:              <computed>
         network.0.name:             "mynetwork"
         network.0.port:             <computed>
         network.0.uuid:             <computed>
         power_state:                "active"
         region:                     <computed>
         security_groups.#:          "1"
         security_groups.1479987471: "security-grp"
         stop_before_destroy:        "false"
         user_data:                  "29acc323491f3d18a8e919d9ecb0161925decd63"
   
   Plan: 1 to add, 0 to change, 0 to destroy.
   
   ------------------------------------------------------------------------
   
   Note: You didn't specify an "-out" parameter to save this plan, so Terraform
   can't guarantee that exactly these actions will be performed if
   "terraform apply" is subsequently run.
   ```

1. Run ``terraform apply`` to apply the changes.
   ```
   $ terraform apply
   
   An execution plan has been generated and is shown below.
   Resource actions are indicated with the following symbols:
     + create
   
   Terraform will perform the following actions:
   
     + openstack_compute_instance_v2.web
   ... truncated ...
   
   Plan: 1 to add, 0 to change, 0 to destroy.
   
   Do you want to perform these actions?
     Terraform will perform the actions described above.
     Only 'yes' will be accepted to approve.
   
     Enter a value: yes
   
   openstack_compute_instance_v2.web: Creating...
   ... truncated ...
   openstack_compute_instance_v2.web: Still creating... (10s elapsed)
   openstack_compute_instance_v2.web: Still creating... (20s elapsed)
   openstack_compute_instance_v2.web: Creation complete after 21s (ID: 9e75c039-ca49-42e6-8fde-fc631c6cbe47)
   
   Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
   ```
   
1. Check Openstack Dashboard, ``web-1`` should be present. Attach floating IP to newly created instance, ssh into the instance check the bootstrap log.
   <img src="https://gitlab.com/gchua/terraform/raw/master/imgs/openstack_dashboard.png" height="450" />

   ```
   $ cat /tmp/bootstrap.log
   Docker version 18.09.1, build 4c52b90
   chronyd (chrony) version 3.2 (+CMDMON +NTP +REFCLOCK +RTC +PRIVDROP +SCFILTER +SECHASH +SIGND +ASYNCDNS +IPV6 +DEBUG)
   git version 1.8.3.1
   ```