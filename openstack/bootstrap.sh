#!/bin/bash

FILE=/tmp/bootstrap.log

# Update packages
yum -y update

# Setup Docker Repository
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Docker
yum -y install docker-ce
systemctl start docker
systemctl enable docker
docker --version >> $FILE

# Chrony
yum -y install chrony
systemctl start chronyd
systemctl enable chronyd
chronyd --version >> $FILE

# Timezone
echo "ZONE=\"Asia/Singapore\"" > /etc/sysconfig/clock
ln -sf /usr/share/zoneinfo/Asia/Singapore /etc/localtime
